function getPublicToken(cname) {
    const public_token = cname + '=';
    let cookie_list = document.cookie.split(';');
    for(let cookie_item = 0; cookie_item < cookie_list.length; cookie_item++) {
        let cookie = cookie_list[cookie_item];
        while (cookie.charAt(0) === ' ') {
            cookie = cookie.substring(1);
        }
        if (cookie.indexOf(public_token) === 0) {
            return  cookie.substring(public_token.length,  cookie.length);
        }
    }
    return '';
}

function pollyDecodeToken(){
    const token = getPublicToken('public-token');
    if (token) {
        const parsedToken = token.split('.')[1].replace(/-/g, '+').replace(/_/g, '/');
        return JSON.parse(window.atob(parsedToken));
    } else {
        return {
            'email':'User',
            'name': 'User',
        }
    }
}

email = pollyDecodeToken().email;
name = pollyDecodeToken().name;

if(pollyDecodeToken().email === 'User') {
    window.intercomSettings = {
        "app_id": "ancaour2",
    };
} else {
    window.intercomSettings = {
        "app_id": "ancaour2",
        "email": email,
        "name": name
    };
}

(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/ancaour2';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()