function checkMail(mail){
    var rx = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(rx.test(mail)){
        return true;
    }
    return false;
}

function checkPhone(n){
    var rx = /^\d{10}$/;
    if(rx.test(n)){
        return true;
    }
    return false;
}
$(document).ready(function(){
    $("#send").click(function(e){
      e.preventDefault();
      var name = $("#name").val();
      var email = $("#email").val();
      var phone = $("#phone").val();
      var msg = $("#msg").val();

      if(name=='' && email=='' && phone=='' && msg==''){
        alert("*Please provide a valid details");
    }else if(name==''){
        alert("*Please enter your name!");
    }else if(msg==''){
        alert("*Please enter some message...!");
    }else if(!checkPhone(phone)){
        alert("*Mobile number must be of 10 digits only!");
    }else if(!checkMail(email)){
        alert("*Invalid email address");
    }else{
        $("#send").prop('disabled', true);
        var url = "/sendMail?name="+name+"&email="+email+"&phone="+phone+"&msg="+msg;
        var getRequest = new XMLHttpRequest();
        getRequest.open("get", url, true);
        getRequest.addEventListener("readystatechange", function() {
           if (getRequest.readyState === 4 && getRequest.status === 200) {
             alert(getRequest.responseText);
             $("#name").val(null);$("#email").val(null);$("#phone").val(null);$("#msg").val(null);
             $("#phone").css({'border-color':'#ccc'});
             $("#email").css({'border-color':'#ccc'});
             $("#send").prop('disabled', false);
           }
       });
        getRequest.send();

    }
    });

    $("#email").keyup(function(){
        if(!checkMail($(this).val())){
            $(this).css({'border-color':'red'});
        }else{
            $(this).css({'border-color':'green'});
        }
    });
    $("#phone").keyup(function(){
        var rex = /^\d{10}$/;
        var numbers = /^[0-9]+$/;
        if(!checkPhone($(this).val())){
            $(this).css({'border-color':'red'});
            if(!numbers.test($(this).val())){
                $(this).val(null);
            }
        }else{
            $(this).css({'border-color':'green'});
        }
    });
});
