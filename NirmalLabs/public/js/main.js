(function ($) {
  "use strict";


  // Preloader (if the #preloader div exists)
  $(window).on('load', function () {
    if ($('#preloader').length) {
      $('#preloader').delay(100).fadeOut('slow', function () {
        $(this).remove();
      });
    }
  });


  // Back to top button
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  });
  $('.back-to-top').click(function(){
    $('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo');
    return false;
  });


  // Initiate the wowjs animation library
  new WOW().init();


  // Header scroll class
  $(window).scroll(function() {
    if ($(this).scrollTop() > 90) {
      $('.header').addClass('header-scrolled');
    } else {
      $('.header').removeClass('header-scrolled');
    }
  });

  if ($(window).scrollTop() > 90) {
    $('.header').addClass('header-scrolled');
  }


  // Smooth scroll for the navigation and links with .scrollto classes
  $('.main-nav a, .mobile-nav a, .scrollto').on('click', function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      if (target.length) {
        var top_space = 0;

        if ($('.header').length) {
          top_space = $('.header').outerHeight();

          if (! $('.header').hasClass('header-scrolled')) {
            top_space = top_space - 40;
          }
        }

        $('html, body').animate({
          scrollTop: target.offset().top - top_space
        }, 1500, 'easeInOutExpo');

        if ($(this).parents('.main-nav, .mobile-nav').length) {
          $('.main-nav .active, .mobile-nav .active').removeClass('active');
          $(this).closest('li').addClass('active');
        }

        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('.mobile-nav-toggle i').toggleClass('fa-times fa-bars');
          $('.mobile-nav-overly').fadeOut();
        }
        return false;
      }
    }
  });


  // Navigation active state on scroll
  var nav_sections = $('section');
  var main_nav = $('.main-nav, .mobile-nav');
  var main_nav_height = $('.header').outerHeight();

  $(window).on('scroll', function () {
    var cur_pos = $(this).scrollTop();
  
    nav_sections.each(function() {
      var top = $(this).offset().top - main_nav_height,
          bottom = top + $(this).outerHeight();
  
//      if (cur_pos >= top && cur_pos <= bottom) {
//        main_nav.find('li').removeClass('active');
//        main_nav.find('a[href="#'+$(this).attr('id')+'"]').parent('li').addClass('active');
//      }
    });
  });


  // jQuery counterUp (used in Whu Us section)
  $('[data-toggle="counter-up"]').counterUp({
    delay: 10,
    time: 1000
  });


  // Porfolio isotope and filter
  $(window).on('load', function () {
    var portfolioIsotope = $('.portfolio-container').isotope({
      itemSelector: '.portfolio-item'
    });
    $('#portfolio-flters li').on( 'click', function() {
      $("#portfolio-flters li").removeClass('filter-active');
      $(this).addClass('filter-active');
  
      portfolioIsotope.isotope({ filter: $(this).data('filter') });
    });
  });


  // Testimonials carousel (uses the Owl Carousel library)
  $(".testimonials-carousel").owlCarousel({
    autoplay: false,
    dots: true,
	slideSpeed: 2000,
    paginationSpeed: 500,
	autoplaySpeed:2500,
    loop: true,
    items: 1
  });


  // Clients carousel (uses the Owl Carousel library)
  $(".clients-carousel").owlCarousel({
    autoplay: true,
    dots: true,
    loop: true,
	navigation: true,
    responsive: { 0: { items: 2 }, 768: { items: 4 }, 900: { items: 6 }
    }
  });
  
  
 // Latest Case Studies (uses the Owl Carousel library)
  $(".case-studies").owlCarousel({
    autoplay: true,
    dots: true,
	margin:30,
    loop: true,
	navigation: true,
    responsive: { 0: { items: 1 }, 768: { items: 2 }, 900: { items: 2 }
    }
  });
  
  
   // OUR LEADERSHIP TEAM (uses the Owl Carousel library)
  $(".team_scroll").owlCarousel({
    autoplay: false,
    dots: false,
    loop: false,
	navigation: true,
    responsive: { 0: { items: 1 }, 768: { items: 2 }, 900: { items: 3 }
    }
  });
  
  
     // OUR ADVISOR (uses the Owl Carousel library)
  $(".our_advisor").owlCarousel({
    autoplay: false,
    dots: true,
    loop: true,
	navigation: true,
    responsive: { 0: { items: 1 }, 768: { items: 2 }, 900: { items: 3 }
    }
  });
  
  
  
     // OUR TEAM (uses the Owl Carousel library)
	  $(".ourteam_scroll").owlCarousel({
		autoplay: false,
		dots: true,
		loop: true,
		navigation: true,
		responsive: { 0: { items: 1 }, 768: { items: 1 }, 900: { items: 1 }
		}
	  });
	  
	   // GRAPHICS (uses the Owl Carousel library)
	  $(".graphimg_scroll").owlCarousel({
		autoplay: true,
		dots: true,
		loop: true,
		navigation: true,
		responsive: { 0: { items: 1 }, 768: { items: 1 }, 900: { items: 1 }
		}
	  });
  
  
    // Sub Navigation page scroll  
   $(".sub_tabing .tabs ul li a").click(function(event) {
        event.preventDefault(); 
        var defaultAnchorOffset = 156;
        var anchor = $(this).attr('data-attr-scroll');
        var anchorOffset = $('#'+anchor).attr('data-scroll-offset');
        if (!anchorOffset)
            anchorOffset = defaultAnchorOffset; 

        $('html,body').animate({ 
            scrollTop: $('#'+anchor).offset().top - anchorOffset
        }, 500);        
    });
	
	
	// Sub Navigation stick after scroll  	
	$(window).scroll(function () {
      //if you hard code, then use console
      //.log to determine when you want the 
      //nav bar to stick.  
		($(window).scrollTop())
    if ($(window).scrollTop() > 360) {
      $('#nav_bar').addClass('navbar-fixed');
    }
    if ($(window).scrollTop() < 361) {
      $('#nav_bar').removeClass('navbar-fixed');
    }
  });
	
	
  
  
  // popup area  
	/* $("#bio1").click(function(e){
    e.stopPropagation();
    $("#data_popup1").show("fast");
	});
	
	$("#bio2").click(function(e){
		e.stopPropagation();
		$("#data_popup2").show("fast");
	});
	
	$("#bio3").click(function(e){
		e.stopPropagation();
		$("#data_popup3").show("fast");
	});
	
	$("#bio4").click(function(e){
		e.stopPropagation();
		$("#data_popup4").show("fast");
	});
	
	$("#bio5").click(function(e){
		e.stopPropagation();
		$("#data_popup5").show("fast");
	});
	
	$("#bio6").click(function(e){
		e.stopPropagation();
		$("#data_popup6").show("fast");
	}); 

	$(document).click(function(e) {
		 if (!(e.target.id === 'data_popup1' || e.target.id === 'data_popup2' || e.target.id === 'data_popup3' || e.target.id === 'data_popup4' || e.target.id === 'data_popup5' || e.target.id === 'data_popup2')) {
		 if (!(e.target.id === 'data_popup1' || e.target.id === 'data_popup2' || e.target.id === 'data_popup3' || e.target.id === 'data_popup4' || e.target.id === 'data_popup5' || e.target.id === 'data_popup2')) {
			 $("#data_popup1, #data_popup2, #data_popup3, #data_popup4, #data_popup5, #data_popup6").hide("fast");                        
		 }
	});*/


	// Scroll area 
		$(window).on('load', function () {
			$("#content-1").mCustomScrollbar({
				theme:"minimal"
			});
		});
		
		
		
		
		/* $('.containt-cli-show').on('click', function(){
			$('.tabs').slideToggle();
		}); */
		
		
		$('.subtab').on('click', function(){
			var $containerWidth = $(window).width();
			if ($containerWidth <= 767) {
				$('.tabs').slideToggle();
			}
		});



	var params = {
        container: document.getElementById('lottie'),
        renderer: 'svg',
        loop: true,
        autoplay: true,
        animationData: animationData
    };

    var anim;
    anim = lottie.loadAnimation(params);

/* Formato del slider */
$(document).ready(function() {
  $('.ba-slider').beforeAfter();
  
});
	
	
		

})(jQuery);


			


